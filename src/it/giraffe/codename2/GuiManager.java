package it.giraffe.codename2;

import it.giraffe.tris.core.TileManager;
import it.giraffe.tris.core.TileValue;
import it.giraffe.tris.core.settings.LocaleUtil;
import it.giraffe.tris.dataStructures.DirectedCouple;
import it.giraffe.tris.ui.painters.WinPainter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Display;
import com.codename1.ui.Label;

// FIXME change static to classic methods!
public class GuiManager {	
	
	private static GuiManager instance = new GuiManager();
	
	private static Map<Integer, Button> buttonsMap;
	private static Map<Label, String> labels;
	private static Map<Command, String> commands;
	
	
	private GuiManager(){
		labels = new HashMap<Label, String>();
		commands = new HashMap<Command, String>();
		buttonsMap = new HashMap<Integer, Button>();
		
	}
	
	public static GuiManager getInstance(){
		return instance;
	}
	
	public /*static*/ void setButtonsMap(Map<Integer, Button> buttonsMap) {
		GuiManager.buttonsMap = buttonsMap;
	}	
	
	/**
	 * Clean the board ignoring possible already taken move by AI
	 */
	@Deprecated
	public /*static*/ void cleanBoard(){		
		for (Integer buttonId : buttonsMap.keySet()) {
			buttonsMap.get(buttonId).setText(" ");
		}
	}
	
	/**
	 * Add to GuiManager a label with the information of the locale string identifier
	 * @param label 
	 * @param localeText String identifier of the label text to allow different languages 
	 */
	public /*static*/ void addLabel(Label label, String localeText){
//		if (labels == null){
//			labels = new HashMap<Label, String>();
//		}
		labels.put(label, localeText);
	}
	
	/**
	 * Add to GuiManager a command with the information of the locale string identifier
	 * @param command 
	 * @param localeText String identifier of the command text to allow different languages 
	 */
	public /*static*/ void addCommand(Command command, String localeText) {
//		if (commands == null){
//			commands = new HashMap<Command, String>();
//		}
		commands.put(command, localeText);
	}
		
	/**
	 * Refresh all the labels texts and commands commandNames according to
	 * current language
	 */
	public /*static*/ void refreshLanguage() {
//		if (labels != null) {
		for (Label label : labels.keySet()) {
			// label.setText(LocaleUtil.getLocalString(labels.get(label)));
			label.setText(labels.get(label));

		}
//		}
//		if (commands != null) {
		for (Command command : commands.keySet()) {
			// String localString = LocaleUtil.getLocalString(commands
			// .get(command));
			String localString = (commands.get(command));
			command.setCommandName(localString);
		}
//		}
	}
	
	/**
	 * Refresh the board according to tileManager board
	 * 
	 * @param manager
	 */
	public /*static*/ void refreshBoard(TileManager manager) {
		for (Integer tileId : buttonsMap.keySet()) {
			TileValue value = manager.getTileValueById(tileId);
			buttonsMap.get(tileId).setText(value.toStringCompact());
			Display.getInstance().getCurrent().setGlassPane(null);
		}
	}

	public /*static*/ void printWinningLine(
			List<DirectedCouple<Integer>> winningsTiles) {
		Display.getInstance().getCurrent()
				.setGlassPane(new WinPainter(winningsTiles, buttonsMap));
	}
}
