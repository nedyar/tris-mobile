package it.giraffe.codename2;

import it.giraffe.tris.core.TileManager;
import it.giraffe.tris.ui.actionListener.ExitActionListener;
import it.giraffe.tris.ui.actionListener.MapFormActionListener;
import it.giraffe.tris.ui.actionListener.MoveListener;
import it.giraffe.tris.ui.actionListener.NewGameActionListener;
import it.giraffe.tris.ui.actionListener.SettingFormActionListener;
import it.giraffe.tris.ui.commands.BackCommand;
import it.giraffe.tris.ui.locale.LocaleButton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

public class MyApplication {

	private Form mainForm;
	private TileManager tileManager;
	private Map<Integer, Button> buttonsMap;
	private Resources resources;

	public void init(Object context) {
		try {
//			resources = Resources.openLayered("/leather");
			resources = Resources.openLayered("/resources");
			
			UIManager.getInstance().setBundle(resources.getL10N("Locale", "it"));

			UIManager.getInstance().setThemeProps(
				resources.getTheme(resources.getThemeResourceNames()[0]));

			/* Language */
//			LocaleUtil.setResources(resources);
//			LocaleUtil.setLanguage("it");

		} catch (IOException e) {
			e.printStackTrace();
		}
		tileManager = new TileManager();

		// Pro users - uncomment this code to get crash reports sent to you
		// automatically
		
//		Display.getInstance().addEdtErrorHandler(new ActionListener() {
//			public void actionPerformed(ActionEvent evt) {
//				evt.consume();
//				Log.p("Exception in AppName version "
//						+ Display.getInstance().getProperty("AppVersion",
//								"Unknown"));
//				Log.p("OS " + Display.getInstance().getPlatformName());
//				Log.p("Error " + evt.getSource());
//				Log.p("Current Form "
//						+ Display.getInstance().getCurrent().getName());
//				Log.e((Throwable) evt.getSource());
//				Log.sendLog();
//			}
//		});
		 
	}

	public void start() {
		if (mainForm != null) {
			mainForm.show();
			return;
		}
		
		/* Main Form */
		mainForm = new Form("TicTacToe");
		mainForm.setScrollable(false);
		
		// TODO
//		mainForm.addOrientationListener(null);
//		mainForm.paint();
//		Graphic

		int dimension = tileManager.getDimension();

		/* Top Menu */
		FlowLayout flowLayout = new FlowLayout();
		Container topMenu = new Container(flowLayout);

		// New Game
		LocaleButton newGame = new LocaleButton("new_game");
//		Button newGame = new Button("new_game");
		newGame.addActionListener(new NewGameActionListener(tileManager,
				buttonsMap));
		topMenu.addComponent(newGame);

		// Exit
		LocaleButton exit = new LocaleButton("exit");
//		Button exit = new Button("exit");
		exit.addActionListener(new ExitActionListener());
		topMenu.addComponent(exit);

		// Settings
		LocaleButton settingsButton = new LocaleButton("settings");
//		Button settingsButton = new Button("settings");	
		settingsButton.addActionListener(new SettingFormActionListener(mainForm));
		topMenu.addComponent(settingsButton);
		
		
		
		// Test locale description
//		Button testLocaleButton = new Button("tie");
//		testLocaleButton.addActionListener(new SettingFormActionListener(mainForm));
//		topMenu.addComponent(testLocaleButton);
//		
		
		// Test Map
		LocaleButton mapButton = new LocaleButton("map");
//		Button settingsButton = new Button("settings");
		mapButton.addActionListener(new MapFormActionListener(mainForm));
		topMenu.addComponent(mapButton);
		
		
		
		
		// topMenu to mainForm
		mainForm.addComponent(topMenu);
		
		

		
		// Test Send SMS
//		Button sendSmsButton = new Button("Send SMS");
//		topMenu.addComponent(sendSmsButton);
//		sendSmsButton.addActionListener(new ActionListener() {			
//			@Override
//			public void actionPerformed(ActionEvent evt) {				
//				try {
//					Display.getInstance().sendSMS("3464758402", "hi!");
//					Dialog.show("Successo", "Messaggio inviato con successo", "ok", null);
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					Dialog.show("Errore", "Impossibile inviare il messaggio", "ok", null);
//					e.printStackTrace();
//				}
//			}
//		});
		


		/* Board */
		GridLayout gridLayout = new GridLayout(dimension, dimension);
		// TableLayout tableLayout = new TableLayout(dimension, dimension);

//		 Container board = new Container(tableLayout);
		Container board = new Container(gridLayout);
//		BoardContainer board = new BoardContainer(gridLayout);

		buttonsMap = new HashMap<Integer, Button>();
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				Integer tempId = tileManager.getTileIdByPosition(i, j);

				Button button = new Button(" ");
				
				// FIXME use instead:
//				Display.getInstance().getDisplayHeight();
//				Display.getInstance().getDisplayWidth();
				
				button.setPreferredSize(new Dimension(150, 150));

				MoveListener moveListener = new MoveListener(tileManager,
						tempId, buttonsMap);
				button.addActionListener(moveListener);

				buttonsMap.put(tempId, button);
				board.addComponent(button);
			}
		}

		/* Back command */
		Command backCommand = new BackCommand(null);

		mainForm.setBackCommand(backCommand);
		mainForm.addComponent(board);
		mainForm.show();

		/* Altezza uguale a larghezza */
		for (Integer buttonId : buttonsMap.keySet()) {
			Button button = buttonsMap.get(buttonId);
			int width = button.getWidth();
			button.setPreferredH(width);
		}
		
		GuiManager.getInstance().setButtonsMap(buttonsMap);
		GuiManager.getInstance().refreshBoard(tileManager);
	}

	public void stop() {
		mainForm = Display.getInstance().getCurrent();
	}

	public void destroy() {

	}
}
