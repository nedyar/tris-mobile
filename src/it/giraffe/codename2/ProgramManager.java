package it.giraffe.codename2;

import it.giraffe.tris.core.settings.Settings;

// XXX Valuto se toglierlo
public class ProgramManager {

	private static ProgramManager instance = new ProgramManager();
	
	private Settings settings;
	
	private ProgramManager() {
		this.settings = Settings.getInstance();
	}
	
	
	public static ProgramManager getInstance(){
		return instance;
	}
	
	public Settings getSettings(){
		return this.settings;
	}
}
