package it.giraffe.tris.map;

public enum Color {
	
	BLACK, WHITE;
//	BLACK(0), WHITE(1);
	
//	private Color(int )
	
	public Color getOpposite(){
		switch (this) {
		case BLACK:
			return WHITE;		
		case WHITE:
			return BLACK;
		default:
			return null;
		}
	}
	
	public Integer getIntValue(){
		switch (this) {
		case BLACK:
			return 0;		
		case WHITE:
			return 0xffffff;
		default:
			return null;
		}
	}
}
