package it.giraffe.tris.map;

import com.codename1.maps.BoundingBox;
import com.codename1.maps.Coord;
import com.codename1.maps.Mercator;
import com.codename1.maps.Projection;
import com.codename1.maps.Tile;
import com.codename1.maps.layers.AbstractLayer;
import com.codename1.ui.Graphics;

public class CircleGradientLayer extends AbstractLayer {


	private Color color;
	
	public CircleGradientLayer(Projection p, String name, Color color) {
		super(p, name);
		this.color = color;
	}
	
	public CircleGradientLayer(Color color){
		super(new Mercator(), "");
		this.color = color;
	}
	
	public CircleGradientLayer(String name){
		super(new Mercator(), name);
	}
	
	

	@Override
	public void paint(Graphics g, Tile screenTile) {	
		g.setAlpha(10);
		g.fillRadialGradient(color.getIntValue(), color.getOpposite()
				.getIntValue(), 100, 100, 100, 100);

	}

	@Override
	public BoundingBox boundingBox() {
		// TODO Auto-generated method stub.
		Coord southWest = new Coord(50,50);
		Coord northEast = new Coord(80,80);
		return new BoundingBox(southWest, northEast);

	}

}
