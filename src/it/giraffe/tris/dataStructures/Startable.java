package it.giraffe.tris.dataStructures;

public abstract interface Startable<T> {
	public T getStart();
}
