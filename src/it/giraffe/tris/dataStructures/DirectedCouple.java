/**
 * 
 */
package it.giraffe.tris.dataStructures;

/**
 * @author Stefano
 * @param <T>
 *
 */
public class DirectedCouple<T> implements Directable<T> {

//	private double radialDegree;
	private T start;
	private T end;
	
	public DirectedCouple(T start, T end){
		this.start = start;
		this.end = end;
	}
	
	@Override
	public T getStart() {
		// TODO Auto-generated method stub
		return this.start;
	}

	@Override
	public T getEnd() {
		// TODO Auto-generated method stub
		return this.end;
	}

//	public 

}
