package it.giraffe.tris.dataStructures;

public abstract interface Endable<T> {
	public T getEnd();
}
