/**
 * 
 */
package it.giraffe.tris.dataStructures;

/**
 * @author Stefano
 *
 */
public interface Directable<T> extends Startable<T>, Endable<T> {
	
}
