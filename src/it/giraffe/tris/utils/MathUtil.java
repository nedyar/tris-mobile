package it.giraffe.tris.utils;

import java.util.List;
import java.util.Random;

/**
 * Provide some useful method and variable needed in mathematical operations.
 * @author Stefano
 *
 */
public class MathUtil {
	
	private Random random; 
	
	public MathUtil(){
		random = new Random();
	}
	
	public Boolean getRandomBoolean(){
		Double randomDouble = random.nextDouble();
		if (randomDouble < 0.5){
			return true;
		}
		return false;
	}
	
	public <T> T getRandomFromCollection(List<T> collection){
		int size = collection.size();
		int randomInt = random.nextInt(size);
		try {		
			return collection.get(randomInt);
		}
		catch (Exception e) {
			return null;
		}
	}
}
