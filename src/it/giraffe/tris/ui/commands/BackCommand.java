package it.giraffe.tris.ui.commands;

import it.giraffe.tris.ui.dialogs.DialogUtil;
import it.giraffe.tris.ui.locale.LocaleCommand;

import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionEvent;

public class BackCommand extends LocaleCommand {

	private Form fatherForm;

	public BackCommand(Form fatherForm) {
		super((fatherForm == null) ? "exit" : "back");
//		String localeText = "back";
//		if (fatherForm == null){
//			localeText = "exit";
//		}
//		GuiManager.addCommand(this, localeText)
		this.fatherForm = fatherForm;
	}

	public void actionPerformed(ActionEvent ev) {
		if (fatherForm != null) {
			fatherForm.showBack();
		} else {
			if (Display.getInstance().isAllowMinimizing()) {				
				Display.getInstance().minimizeApplication();
			}
			else {
				DialogUtil.showExitDialog();
			}
		}
	}
}
