package it.giraffe.tris.ui.dialogs;

import it.giraffe.tris.core.settings.LocaleUtil;

import com.codename1.ui.Dialog;
import com.codename1.ui.Display;

/**
 * This class provide some method to show common dialog used in the application.
 * @author Stefano
 *
 */
public class DialogUtil {
	
	/**
	 * Show a dialog containig a 
	 */
	public static void showExitDialog() {
//		if (Dialog.show(LocaleUtil.getLocalString("exit"),
//				LocaleUtil.getLocalString("sure_?"),
//				LocaleUtil.getLocalString("yes"),
//				LocaleUtil.getLocalString("no"))) {
		if (Dialog.show("exit",
				"sure_?", "yes", "no")) {
			Display.getInstance().exitApplication();
		}
	}

	/**
	 * Show a classical Yes/No dialog
	 * @param localeTitle String identifier of dialog's title.
	 * @param localeText String identifier of dialog's message.
	 * @param localeTextParams String identifier of dialog's message parameters
	 * @return Returns true if the OK button was pressed.
	 */
	public static boolean showYesNoDialog(String localeTitle,
			String localeText, String...localeTextParams) {
//		return Dialog.show(LocaleUtil.getLocalString(localeTitle),
//				LocaleUtil.getLocalString(localeText, localeTextParams),
//				LocaleUtil.getLocalString("yes"),
//				LocaleUtil.getLocalString("no"));
		return Dialog.show(localeTitle,
				LocaleUtil.getLocalStringParametic(localeText, localeTextParams),
				"yes", "no");
	}
}
