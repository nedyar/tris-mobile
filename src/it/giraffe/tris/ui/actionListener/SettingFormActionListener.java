package it.giraffe.tris.ui.actionListener;

import it.giraffe.codename2.GuiManager;
import it.giraffe.codename2.ProgramManager;
import it.giraffe.tris.core.settings.Languages;
import it.giraffe.tris.core.settings.LocaleUtil;
import it.giraffe.tris.core.settings.Settings;
import it.giraffe.tris.ui.commands.BackCommand;

import java.util.HashMap;
import java.util.Map;

import com.codename1.components.OnOffSwitch;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;

public class SettingFormActionListener extends NewFormActionListener {

	public SettingFormActionListener(Form fatherForm) {
		super(fatherForm);
	}

	public void actionPerformed(ActionEvent evt) {
		Form settingsForm = new Form("settings");
//		Form settingsForm = new Form(LocaleUtil.getLocalString("settings"));
		
		BoxLayout boxLayout = new BoxLayout(BoxLayout.X_AXIS);
		
	
		
		final Settings entrySetting = ProgramManager.getInstance().getSettings();
		final Settings copyEntrySettings = entrySetting.copy();
		
		/* Languages */
		Container languageContainer = new Container();
		languageContainer.setLayout(boxLayout);
		settingsForm.addComponent(languageContainer);
		
		// TODO totest
//		Label languageLabel = new Label(LocaleUtil.getLocalString("language") + ":");
		Label languageLabel = new Label("language"/* + ":"*/);
		
		
		languageContainer.addComponent(languageLabel);
		final ButtonGroup buttonGroupLanguages = new ButtonGroup();

		final Map<Integer, String> languagesMap = new HashMap<Integer, String>();
		
		String language = LocaleUtil.getInstance().getLanguage();		
		int index = 0;
		for (final Languages locale : Languages.values()) {
			RadioButton tempChangeLanguage = new RadioButton(locale.getValue());
			if (locale.getId().equals(language)){
				tempChangeLanguage.setSelected(true);
			}
			languagesMap.put(index, locale.getId());
			buttonGroupLanguages.add(tempChangeLanguage);
			languageContainer.addComponent(tempChangeLanguage);
			index++;
		}		
		
		/* AI */
		Container aiContainer = new Container();
//		BoxLayout boxLayout = new BoxLayout(BoxLayout.X_AXIS);
		aiContainer.setLayout(boxLayout);
		settingsForm.addComponent(aiContainer);
		
		// TODO da testare la traduzione, forse i due punti ...
//		Label aiLabel = new Label(LocaleUtil.getLocalString("enable_ai") + ":");
		Label aiLabel = new Label("enable_ai"/* + ":"*/);

		aiContainer.addComponent(aiLabel);
		
		
		// Try with onOff
		final OnOffSwitch enableAiOnOff = new OnOffSwitch();
		enableAiOnOff.setOn("on");
		enableAiOnOff.setOff("off");
		enableAiOnOff.setValue(entrySetting.isAiEnabled());
		aiContainer.addComponent(enableAiOnOff);
		
		
//		final CheckBox enabledAiCheckBox = new CheckBox();
//		enabledAiCheckBox.setSelected(entrySetting.isAiEnabled()/*Settings.isAiEnabled()*/);
//		aiContainer.addComponent(enabledAiCheckBox);

//		final Map<Integer, String> languagesMap = new HashMap<Integer, String>();
//		
//		String language = LocaleUtil.getLanguage();		
//		int index = 0;
//		for (final Languages locale : Languages.values()) {
//			RadioButton tempChangeLanguage = new RadioButton(locale.getValue());
//			if (locale.getId().equals(language)){
//				tempChangeLanguage.setSelected(true);
//			}
//			languagesMap.put(index, locale.getId());
//			buttonGroupLanguages.add(tempChangeLanguage);
//			aiContainer.addComponent(tempChangeLanguage);
//			index++;
//		}	
		
		/* Apply */
		Container applyContainer = new Container();
		applyContainer.setLayout(boxLayout);
		settingsForm.addComponent(applyContainer);
		
//		Button saveSettingsButton = new Button(LocaleUtil.getLocalString("save"));
		Button saveSettingsButton = new Button("save");
		saveSettingsButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent evt) {
				int selectedIndex = buttonGroupLanguages.getSelectedIndex();	
				
				LocaleUtil.getInstance().setLanguage(languagesMap.get(selectedIndex));
//				Settings.getInstance().setLanguage(languagesMap.get(selectedIndex));

				
				
				//  refresh graphics strings
				GuiManager.getInstance().refreshLanguage();
				
				boolean tempAiEnabled =
						enableAiOnOff.isValue();
//						enabledAiCheckBox.isSelected();
				entrySetting.setAiEnabled(tempAiEnabled);
				
				// TODO show previous form
				fatherForm.showBack();
				
			}
		});	
		applyContainer.addComponent(saveSettingsButton);
		
		// Reset
//		Button resetSettingsButton = new Button(LocaleUtil.getLocalString("reset"));
		Button resetSettingsButton = new Button("reset");

		
		resetSettingsButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent evt) {
				entrySetting.restoreTo(copyEntrySettings);
				
				enableAiOnOff.setValue(entrySetting.isAiEnabled());
//				enabledAiCheckBox.setSelected(entrySetting.isAiEnabled());
				
				// get language button group index from value
				int tempIndex = -1;
				for (Integer key : languagesMap.keySet()) {
					if (languagesMap.get(key).equals(entrySetting.getLanguage())){
						tempIndex = key;
					}
				}
				if (tempIndex != -1){
					buttonGroupLanguages.setSelected(tempIndex);
				}		
			}
		});	
		applyContainer.addComponent(resetSettingsButton);
		
		// Cancel
//		Button cancelSettingsButton = new Button(LocaleUtil.getLocalString("cancel"));
		Button cancelSettingsButton = new Button("cancel");
		
		cancelSettingsButton.addActionListener(new BackCommand(fatherForm));
		applyContainer.addComponent(cancelSettingsButton);
		

		settingsForm.setBackCommand(new BackCommand(fatherForm));
		settingsForm.show();
	}
}
