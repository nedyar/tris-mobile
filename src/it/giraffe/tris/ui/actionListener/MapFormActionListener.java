package it.giraffe.tris.ui.actionListener;

import it.giraffe.tris.map.CircleGradientLayer;
import it.giraffe.tris.map.Color;
import it.giraffe.tris.ui.commands.BackCommand;

import java.io.IOException;

import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.maps.MapComponent;
import com.codename1.maps.Mercator;
import com.codename1.maps.layers.ArrowLinesLayer;
import com.codename1.maps.layers.Layer;
import com.codename1.maps.layers.PointLayer;
import com.codename1.maps.layers.PointsLayer;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;

public class MapFormActionListener extends NewFormActionListener {

	private Coord lastLocation;
	
	public MapFormActionListener(Form fatherForm) {
		super(fatherForm);
		lastLocation = new Coord(45.5972, 9.2864);
	}

	public void actionPerformed(ActionEvent evt) {
		Form mapForm = new Form("map");
//		Form settingsForm = new Form(LocaleUtil.getLocalString("settings"));
		
		
		mapForm.setLayout(new BorderLayout());
		mapForm.setScrollable(false);
		
//		GoogleMapsProvider googleMapsProvider = new GoogleMapsProvider("aaaa");
		
	    final MapComponent mc = new MapComponent();

//        putMeOnMap(mc);
        
        this.drawGradient(mc);
        
        mc.zoomToLayers();

        mapForm.addComponent(BorderLayout.CENTER, mc);
        
        mc.addLayer(new ArrowLinesLayer("arrowLine"));
        
//        mapForm.addCommand(new BackCommand());
        mapForm.setBackCommand(new BackCommand(mapForm));
        mapForm.show();

				
//		MapProvider mapProvider = new OpenStreetMapProvider();//new GoogleMapsProvider(apiKey);
//		mapProvider.translate(, 3, 500, 500);
		


		mapForm.setBackCommand(new BackCommand(super.fatherForm));
		mapForm.show();
	}
	
	private void drawGradient(MapComponent map){
		
//		Projection projection = new 
		Layer layer = new CircleGradientLayer(new Mercator(),
				"try", Color.WHITE);		
		map.addLayer(layer);
	}
	
	private void putMeOnMap(MapComponent map) {
		try {
			Location loc = LocationManager.getLocationManager()
					.getCurrentLocation();
			lastLocation = new Coord(loc.getLatitude(), loc.getLongitude());
			Image i = Image.createImage("/blue_pin.png");

			PointsLayer pl = new PointsLayer();
			pl.setPointIcon(i);
			PointLayer p = new PointLayer(lastLocation, "You Are Here", i);
			p.setDisplayName(true);
			pl.addPoint(p);
			pl.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					PointLayer p = (PointLayer) evt.getSource();
					System.out.println("pressed " + p);

					Dialog.show(
							"Details",
							"You Are Here" + "\n" + p.getLatitude() + "|"
									+ p.getLongitude(), "Ok", null);
				}
			});
			map.addLayer(pl);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
