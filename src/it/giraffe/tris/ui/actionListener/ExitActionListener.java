package it.giraffe.tris.ui.actionListener;

import it.giraffe.tris.ui.dialogs.DialogUtil;

import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

public final class ExitActionListener implements ActionListener {

	public void actionPerformed(ActionEvent evt) {
		DialogUtil.showExitDialog();
	}
}