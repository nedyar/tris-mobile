package it.giraffe.tris.ui.actionListener;

import it.giraffe.codename2.GuiManager;
import it.giraffe.tris.core.TileManager;
import it.giraffe.tris.core.TileValue;
import it.giraffe.tris.core.settings.LocaleUtil;
import it.giraffe.tris.core.settings.Settings;
import it.giraffe.tris.ui.dialogs.DialogUtil;

import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

public class MoveListener implements ActionListener {

	private TileManager tileManager;
	private int tileId;
	Map<Integer, Button> buttonsMap;

	public MoveListener(TileManager tileManager, int tileId,
			Map<Integer, Button> buttonsMap) {
		this.tileManager = tileManager;
		this.tileId = tileId;
		this.buttonsMap = buttonsMap;
	}

	public void actionPerformed(ActionEvent evt) {
		TileValue tileValue = tileManager.useTile(this.tileId);
		if (tileValue != null) {
			// DONE maybe use GuiManager instead (Is it slower?)
			GuiManager.getInstance().refreshBoard(tileManager);
			// buttonsMap.get(tileId).setText(tileValue.toStringCompact());

//			if (tileManager.checkWin()) {
			if (!tileManager.getWinningsTiles().isEmpty()){
				// TODO show a graphical effect
				GuiManager.getInstance().printWinningLine(tileManager.getWinningsTiles());
				startNewGameDialog("win", "win_messagge", tileValue.toString());
			} else if (tileManager.checkTie()) {
				// TODO show a graphical effect
				startNewGameDialog("tie", "tie_messagge");
			} else {
				/* Execute the PC Move */
				if (Settings.getInstance().isAiEnabled()) {
					int nextIndexes[] = tileManager.nextMove();
					if (nextIndexes == null) {
						System.err.println("AI error!");
					}
					int tilePCid = tileManager.getTileIdByPosition(
							nextIndexes[0], nextIndexes[1]);
					tileValue = tileManager.useTile(tilePCid);
					if (tileValue != null) {

						// TODO maybe use GuiManager instead
						GuiManager.getInstance().refreshBoard(tileManager);
						// buttonsMap.get(tilePCid).setText(tileValue.toStringCompact());

//						if (tileManager.checkWin()) {
						if (!tileManager.getWinningsTiles().isEmpty()){
							// TODO show a graphical effect
//							GuiManager.printWinningLine(tileManager);
							GuiManager.getInstance().printWinningLine(tileManager.getWinningsTiles());
							startNewGameDialog("loose", "loose_messagge",
									tileValue.toString());
						} else if (tileManager.checkTie()) {
							// TODO show a graphical effect
							startNewGameDialog("tie", "tie_messagge");
						}
					}
				}
			}
		} 
		else if (!tileManager.isGameInProgress()) {
			this.startNewGameDialog("play_again_title", "play_again_message");
		}
		else {
//			Dialog.show(LocaleUtil.getLocalString("move"),
//					LocaleUtil.getLocalString("forbidden_move"),
//					LocaleUtil.getLocalString("ok"), null);
			Dialog.show("move", "forbidden_move", "ok", null);
		}
	}

	private void startNewGameDialog(String localeTitle, String localeText,
			String... localeTextParams) {

		if (DialogUtil.showYesNoDialog(localeTitle, localeText,
				localeTextParams)) {
			tileManager.initBoard();
			GuiManager.getInstance().refreshBoard(tileManager);

		} else {
			tileManager.stopGame();
		}
	}
}
