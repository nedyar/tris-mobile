package it.giraffe.tris.ui.actionListener;

import it.giraffe.codename2.GuiManager;
import it.giraffe.tris.core.TileManager;
import it.giraffe.tris.core.settings.LocaleUtil;
import it.giraffe.tris.ui.dialogs.DialogUtil;

import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;


public class NewGameActionListener implements ActionListener {
	
	public NewGameActionListener(TileManager tileManager,
			Map<Integer, Button> buttonsMap) {
		super();
		this.tileManager = tileManager;
	}

	private TileManager tileManager;
	
	
	public void actionPerformed(ActionEvent evt) {
//		if (Dialog.show(LocaleUtil.getLocalString("new_game"),
//				LocaleUtil.getLocalString("sure_?"),
//				LocaleUtil.getLocalString("yes"),
//				LocaleUtil.getLocalString("no"))) {
		if (DialogUtil.showYesNoDialog("new_game", "sure_?")) {
			tileManager.initBoard();
//			GuiManager.cleanBoard();			
			GuiManager.getInstance().refreshBoard(tileManager);
		}
	}
}
