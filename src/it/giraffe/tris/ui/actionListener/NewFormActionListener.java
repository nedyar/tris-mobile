package it.giraffe.tris.ui.actionListener;

import com.codename1.ui.Form;
import com.codename1.ui.events.ActionListener;

public abstract class NewFormActionListener implements ActionListener {

	protected Form fatherForm;
		
	public NewFormActionListener(Form fatherForm) {
		super();
		this.fatherForm = fatherForm;
	}
}
