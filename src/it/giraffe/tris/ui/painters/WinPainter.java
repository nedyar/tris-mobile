package it.giraffe.tris.ui.painters;

import it.giraffe.tris.dataStructures.DirectedCouple;
import it.giraffe.tris.utils.Axis;

import java.util.List;
import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Graphics;
import com.codename1.ui.Painter;
import com.codename1.ui.geom.Rectangle;

public class WinPainter implements Painter {

	private int THICKENESS = 10;
	private List<DirectedCouple<Integer>> couples;

	private Map<Integer, Button> buttonsMap;


	public WinPainter(List<DirectedCouple<Integer>> couples, Map<Integer, Button> buttonsMap) {
		this.couples = couples;
		this.buttonsMap = buttonsMap;
	}
	
	@Override
	public void paint(Graphics g, Rectangle rect) {
		if (couples != null && !couples.isEmpty()) {
			for (DirectedCouple<Integer> couple : couples) {
				Button startingButton = buttonsMap.get(couple.getStart());
				Button endingButton = buttonsMap.get(couple.getEnd());
				
				int startX = startingButton.getAbsoluteX() + (startingButton.getWidth() / 2);
				int startY = startingButton.getAbsoluteY() + (startingButton.getHeight() / 2);
				
				int endX = endingButton.getAbsoluteX() + (endingButton.getWidth() / 2);
				int endY = endingButton.getAbsoluteY() + (endingButton.getHeight() / 2);

				
				//TODO
				g.setColor(0x00ff00);
				
//				g.setAntiAliased(true);
				// setting the alpha modify the alpha for the whole form
//				g.setAlpha(240);

				int xPoints[] = this.calculatePoly(startX, endX, Axis.X);
				int yPoints[] = this.calculatePoly(startY, endY, Axis.Y);
				g.fillPolygon(xPoints, yPoints, Math.min(xPoints.length, yPoints.length));


				g.setColor(0xff0000);
				g.drawLine(startX, startY, endX, endY);
				
				
			}
		}
	}
	
	// FIXME to improve!
	private int[] calculatePoly(int start, int end, Axis axis) {
		int out[] = new int[4];
		int delta;
		if (start == end) {
			delta = 0;
		} else {
			delta = THICKENESS / 2;
		}
		if (axis.equals(Axis.X)) {
			out[0] = start - delta;
			out[1] = start + delta;
			out[2] = end + delta;
			out[3] = end - delta;

		} else {
			out[0] = start + delta;
			out[1] = start - delta;
			out[2] = end - delta;
			out[3] = end + delta;
		}
		return out;
	}
}
