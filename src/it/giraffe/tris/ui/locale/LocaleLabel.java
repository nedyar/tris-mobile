package it.giraffe.tris.ui.locale;

import it.giraffe.codename2.GuiManager;

import com.codename1.ui.Label;

/**
 * This class extends codenameOne Label to automatic manage refresh issue when changing languages
 * @author Stefano
 *
 */
public class LocaleLabel extends Label {
	
	/**
	 * This constructor automatic add this Label to labelsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 */
	public LocaleLabel(String localeText) {
//		super(LocaleUtil.getLocalString(localeText));
		super(localeText);
		GuiManager.getInstance().addLabel(this, localeText);
	}
}
