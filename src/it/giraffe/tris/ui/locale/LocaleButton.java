package it.giraffe.tris.ui.locale;

import it.giraffe.codename2.GuiManager;

import com.codename1.ui.Button;
import com.codename1.ui.Image;

/**
 * This class extends codenameOne Button to automatic manage refresh issue when changing languages
 * @author Stefano
 *
 */
public class LocaleButton extends Button {

	/**
	 * This constructor automatic add this Button to labelsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 * @param icon (See super())
	 */
	public LocaleButton(String localeText, Image icon) {
//		super(LocaleUtil.getLocalString(localeText), icon);
		super(localeText, icon);
		GuiManager.getInstance().addLabel(this, localeText);
	}

	/**
	 * This constructor automatic add this Button to labelsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 */
	public LocaleButton(String localeText) {		
//		super(LocaleUtil.getLocalString(localeText));
		super(localeText);
		GuiManager.getInstance().addLabel(this, localeText);
	}
}
