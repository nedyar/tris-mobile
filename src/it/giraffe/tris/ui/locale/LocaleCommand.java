package it.giraffe.tris.ui.locale;

import it.giraffe.codename2.GuiManager;

import com.codename1.ui.Command;
import com.codename1.ui.Image;

/**
 * This class extends codenameOne Command to automatic manage refresh issue when changing languages
 * @author Stefano
 *
 */
public class LocaleCommand extends Command {

	/**
	 * This constructor automatic add this Command to commandsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 * @param icon (See super())
	 * @param id (See super())
	 */
	public LocaleCommand(String localeText, Image icon, int id) {
//		super(LocaleUtil.getLocalString(localeText), icon, id);
		super(localeText, icon, id);
		GuiManager.getInstance().addCommand(this, localeText);
	}

	/**
	 * This constructor automatic add this Command to commandsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 * @param icon (See super())
	 */
	public LocaleCommand(String localeText, Image icon) {
//		super(LocaleUtil.getLocalString(localeText), icon);
		super(localeText, icon);
		GuiManager.getInstance().addCommand(this, localeText);
	}

	/**
	 * This constructor automatic add this Command to commandsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 * @param id (See super())
	 */
	public LocaleCommand(String localeText, int id) {
//		super(LocaleUtil.getLocalString(localeText), id);
		super(localeText, id);
		GuiManager.getInstance().addCommand(this, localeText);
	}

	/**
	 * This constructor automatic add this Command to commandsMap in the GuiManager that contains
	 * text to be automatically translated.
	 * @param localeText The string identifier used for translation.
	 */
	public LocaleCommand(String localeText) {
//		super(LocaleUtil.getLocalString(localeText));
		super(localeText);
		GuiManager.getInstance().addCommand(this, localeText);
	}
}
