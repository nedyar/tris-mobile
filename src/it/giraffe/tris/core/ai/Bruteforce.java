package it.giraffe.tris.core.ai;

import it.giraffe.tris.core.TileValue;
import it.giraffe.tris.core.Tiles;

public class Bruteforce extends AIPlayer {

	private boolean checkedTile[][];
	
	public Bruteforce(Tiles tiles) {
		super(tiles);
		
		checkedTile = new boolean[ROWS][COLS];
	}

	@Override
	public int[] move() {
	
		int randomTile[] = this.getRandomTile();
		if (randomTile != null){
			int x = randomTile[0];
			int y = randomTile[1];
//			super.
		}
		else {
			// no more moves... checks
		}
		return null;
	}
	
	private int[] getRandomTile() {
		int out[] = new int[2];
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				if (!checkedTile[i][j]
						&& tiles.getTileValueByPosition(i, j).equals(
								TileValue.EMPTY)) {
					out[0] = i;
					out[1] = j;
					return out;
				}
			}
		}
		return null;
	}
	
	
	public int[] MiniMax( int[][] gameboard, boolean isCPUTurn, int[] move, int depth ){
	    if (gameboard[1][1]==TileValue.EMPTY.getInt()) { // Always take middle if possible
	        int[] best = {2,1,1};
	        return best;
	    }

	    // Recursion base-case
	    int winner = -1;//COLS checkWinner();
	    boolean cpuWin =  super.hasWon(mySeed);
	    boolean humanWin =  super.hasWon(oppSeed);
	    
	    
	    if (cpuWin) {
	    	winner = 2;
	    }
    	else if (humanWin ){
    		winner = 0;
    	}
    	else if (this.getRandomTile() == null){
    		winner = 1;
    	}
	    
	    if ( winner == 1 || winner == 2 || winner == 3 ){ // Game is over
	        int[] returnSet = {-1,-1,-1}; // leeg

	        // adjust scores for Minimax    Player<----Tie---->CPU
	        //                                  0       1       2
	        switch (winner){
	        case 1:
	            returnSet[0] = 0 ; // Player wins
	            break;
	        case 2:
	            returnSet[0] = 2; // CPU wins
	            break;
	        case 3:
	            returnSet[0] = 1; // TIE
	            break;
	        default:            // impossible..
	            returnSet[0] = 1;
	        }
	        // The last move led to the end of the game, return this outcome and move
	        returnSet[1] = move[0];
	        returnSet[2] = move[1];
	        return returnSet;
	    }

	    // A move is still possible, the game's not over yet
	    else{
	        int[] bestMove = null;          // contains the best move found for this turn
	        for (int i=0; i<3; i++){        // check all child nodes/boards
	            for (int j=0; j<3; j++){

	                if( gameboard[i][j] == TileValue.EMPTY.getInt() )              // if the spot's not yet filled
	                {   
	                    if (isCPUTurn) gameboard[i][j] = super.mySeed.getInt();   // this move is CPU's
	                    else gameboard[i][j] = super.oppSeed.getInt();             // this move is Player's

	                    int[] thismove = {i,j}; // {move Row, move Column}

	                    // recursion
	                    int[] result = MiniMax( gameboard, !isCPUTurn, thismove, depth+1);
	                    // result = { winner result, move Row, move Column}
	                    gameboard[i][j] = TileValue.EMPTY.getInt(); // delete last move after recursion

	                    // check if child is best option
	                    if (bestMove == null) // first child is always the best initially
	                        bestMove = result;
	                    else {
	                        if (!isCPUTurn) // for CPU, higher is better
	                        {   if (result[0]>bestMove[0])
	                                bestMove = result;                              
	                            //if (bestMove[0]==2) return bestMove; Pruning thingy
	                        }
	                        else if (isCPUTurn) // for Player, lower is better
	                        {   if (result[0]<bestMove[0])
	                                bestMove = result;
	                            //if (bestMove[0]==0) return bestMove; Pruning thingy
	                        }   
	                    }

	                }
	            }
	        }
	        return bestMove; // returns the best move found after checking all possible childs
	    }

	}
}
