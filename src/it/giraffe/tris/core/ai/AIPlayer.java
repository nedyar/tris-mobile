package it.giraffe.tris.core.ai;

import it.giraffe.tris.core.TileValue;
import it.giraffe.tris.core.Tiles;

/**
 * Abstract superclass for all AI players with different strategies.
 * To construct an AI player:
 * 1. Construct an instance (of its subclass) with the game Board
 * 2. Call setSeed() to set the computer's seed
 * 3. Call move() which returns the next move in an int[2] array of {row, col}.
 *
 * The implementation subclasses need to override abstract method move().
 * They shall not modify Cell[][], i.e., no side effect expected.
 * Assume that next move is available, i.e., not game-over yet.
 */
public abstract class AIPlayer {
   protected int ROWS = Tiles.DIMENSION;  // number of rows
   protected int COLS = Tiles.DIMENSION;  // number of columns
 
   protected Tiles tiles; // the board's ROWS-by-COLS array of Cells
   protected TileValue mySeed;    // computer's seed
   protected TileValue oppSeed;   // opponent's seed
   
//private int[] winningPatterns = {
//         0b111000000, 0b000111000, 0b000000111, // rows
//         0b100100100, 0b010010010, 0b001001001, // cols
//         0b100010001, 0b001010100               // diagonals
//   };
 
   /** Constructor with reference to game board */
   public AIPlayer(Tiles board) {
      tiles = board;
   }
 
   /** Set/change the seed used by computer and opponent */
   public void setSeed(TileValue seed) {
      this.mySeed = seed;
      oppSeed = mySeed.getOpposite();//(mySeed == TileValue.CROSS) ? TileValue.CIRCLE : TileValue.CROSS;
   }
   
//   public void setBoardValue(int i,int j,int token) {
//		if(i < 0 || i >= 3) return;
//		if(j < 0 || j >= 3) return;
//		tiles[i][j] = token;
//	}
// 
   /** Abstract method to get next move. Return int[2] of {row, col} */
   public abstract int[] move();  // to be implemented by subclasses

/** Returns true if thePlayer wins */
//protected boolean hasWon(TileValue thePlayer) {
//	   int pattern = 0b000000000;  // 9-bit pattern for the 9 cells
//	   for (int row = 0; row < ROWS; ++row) {
//		   for (int col = 0; col < COLS; ++col) {
//			   if (tiles.getTileValueByPosition(row, col).equals(thePlayer)){
//				   //			   if (cells[row][col].content == thePlayer) {
//				   //				   if (tiles.getTileValueByPosition(row, col).equals(thePlayer)){
//				   //				   if (cells[row][col].content == thePlayer) {
//				   pattern |= (1 << (row * COLS + col));
//			   }
//		   }
//	   }
//	   for (int winningPattern : winningPatterns) {
//		   if ((pattern & winningPattern) == winningPattern) return true;
//	   }
//	   return false;
//   }
   
	public boolean hasWon(TileValue player) {
		boolean out = false;
		if (tiles.checkRows(player) || tiles.checkDiags(player) || tiles.checkCols(player)) {
			return true;
		}
		return out;
	}
}