package it.giraffe.tris.core.ai;

import java.util.List;

import it.giraffe.tris.core.MyTile;

public class MyMinMax {

	public int minimax(/* MyTile node, */int depth, boolean maximizingPlayer) {
		int bestValue = 0;

		// TODO
//		if (depth == 0 || gameEnd()) {
//			return 
//					obtainHeuristicValue(node);
//		}
		// if (depth == 0 || isTerminalNode(node)) {
		// return obtainHeuristicValue(node);
		// }
		if (maximizingPlayer) {
			// bestValue := -∞;
			int value = 0;
			bestValue = Integer.MIN_VALUE;

			for (MyTile tile : obtainLegalMoves()) {
				value = minimax(/* tile, */depth - 1, false);
				// val := minimax(child, depth - 1, FALSE);
				// bestValue := max(bestValue, val);
				bestValue = Math.max(bestValue, value);
			}
			return bestValue;
		} else {
			// bestValue := +∞
			bestValue = Integer.MAX_VALUE;
			int value = 0;
			for (MyTile tile : obtainLegalMoves()) {
				// for each child of node {
				value = minimax(/* tile, */depth - 1, true);
				//
				// val := minimax(child, depth - 1, TRUE)
				// bestValue := min(bestValue, val);
				bestValue = Math.min(bestValue, value);
			}
			return bestValue;
		}
	}

	private boolean gameEnd() {
		// TODO Auto-generated method stub
		return false;
	}

	private List<MyTile> obtainLegalMoves() {
		// TODO Auto-generated method stub
		return null;
	}

	private int obtainHeuristicValue(MyTile node) {
		// TODO Auto-generated method stub
		return 0;
	}

	private boolean isTerminalNode(MyTile node) {
		// TODO Auto-generated method stub
		return false;
	}

	/* Initial call for maximizing player */
	public int[] move() {
		minimax(/* origin, */4, true);
		return null;
	}

	// public int MinMax (GamePosition game) {
	// return MaxMove (game);
	// }
	//
	// public int MaxMove (GamePosition game) {
	// if (GameEnded(game) || DepthLimitReached()) {
	// return EvalGameState(game, MAX);
	// }
	// else {
	// best_move <- {};
	// moves <- GenerateMoves(game);
	// ForEach moves {
	// move <- MinMove(ApplyMove(game));
	// if (Value(move) > Value(best_move)) {
	// best_move <- move;
	// }
	// }
	// return best_move;
	// }
	// }
	// MinMove (GamePosition game) {
	// if (GameEnded(game) || DepthLimitReached()) {
	// return EvalGameState(game, MIN);
	// }
	// else {
	// best_move <- {};
	// moves <- GenerateMoves(game);
	// ForEach moves {
	// move <- MaxMove(ApplyMove(game));
	// if (Value(move) > Value(best_move)) {
	// best_move <- move;
	// }
	// }
	// return best_move;
	// }
	// }

}
