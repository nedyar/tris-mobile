package it.giraffe.tris.core;

import it.giraffe.tris.core.settings.LocaleUtil;

/**
 * Represent all the possibles values in a TicTacToe game
 * @author Stefano
 *
 */
public enum TileValue {
	EMPTY, CIRCLE, CROSS;
	
	/**
	 * 
	 * @param value The value on which get opposite.
	 * @return Returns the opposite value by a given one
	 */
	public static TileValue getOpposite(TileValue value){
		switch (value) {
		case EMPTY:
			return null; // TODO random ?!			
		case CIRCLE:
			return CROSS;			
		case CROSS:
			return CIRCLE;
		default:
			return EMPTY;
		}
	}
	
	/**
	 * 
	 * @return Returns the opposite value of one that call.
	 */
	public TileValue getOpposite(){
		switch (this) {
		case EMPTY:
			return null; // TODO random ?!			
		case CIRCLE:
			return CROSS;			
		case CROSS:
			return CIRCLE;
		default:
			return EMPTY;
		}
	}
	
	public String toStringCompact(){
		switch (this) {
		case EMPTY:
			return " ";
		case CIRCLE:
			return "O";			
		case CROSS:
			return "X";
		default:
			return " ";
		}
	}
	
	/**
	 * Custom toString method.
	 */
	@Override
	public String toString(){
		switch (this) {
		case EMPTY:
//			return LocaleUtil.getLocalString("empty");
			return "empty";
		case CIRCLE:
//			return LocaleUtil.getLocalString("circle");
			return "circle";
		case CROSS:
			return "corss";
//			return LocaleUtil.getLocalString("cross");
		default:
			return "null";
		}
	}		
	
	/**
	 * 
	 * @return Returns the integer representation of a value. 
	 * This is used only to compatibility issues with other source code. </br>
	 * EMPTY <-> 0 </br>
	 * CIRCLE <-> 1 </br>
	 * CROSS <-> 2 
	 * 
	 */
	public int getInt(){
		switch (this) {
		case EMPTY:
			return 0;
		case CIRCLE:
			return 1;			
		case CROSS:
			return 2;
		default:
			return -1;
		}
	}	
}
