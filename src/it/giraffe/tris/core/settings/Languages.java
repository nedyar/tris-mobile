package it.giraffe.tris.core.settings;

/**
 * All supported languages!
 * @author Stefano
 *
 */
public enum Languages {
	ENGLISH("en"), ITALIANO("it");
	
	private String id;
	
	private Languages(String id){
		this.id = id;
	}
	
	/**
	 * 
	 * @return Returns language's  id.
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 
	 * @return Return language's well formed string representation.
	 */
	public String getValue() {
		switch (this) {
		case ENGLISH:
			return "English";
			
		case ITALIANO:
			return "Italiano";
			
		default:
			return null;
		}
	}
}
