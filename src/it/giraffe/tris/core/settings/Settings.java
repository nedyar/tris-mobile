package it.giraffe.tris.core.settings;


/**
 * Defines the settings of TicTacToe
 * @author Stefano
 *
 */
// FIXED make method non static, accessible by an instance of Program
public class Settings {
	
	private /*final*/ static Settings instance = new Settings();
	
	//@Setting
	private boolean aiEnabled;
	private String language;
	
	private Settings() {
		// TODO: load from setting file
		aiEnabled = false;
		setLanguage("it");
	}
	
	/**
	 * Enable AI
	 */
	public  void enableAi(){
		aiEnabled = true;
	}
	
	/**
	 * Disable AI
	 */
	public void disableAi(){
		aiEnabled = false;
	}

	/**
	 * @return Returns true iff the ai is Enabled.
	 */
	public boolean isAiEnabled() {
		return aiEnabled;
	}

	/**
	 * Set the AI to the specified value.
	 * @param aiEnabled 
	 */ 
	public void setAiEnabled(boolean aiEnabled) {
		this.aiEnabled = aiEnabled;
	}

	public String getLanguage() {
		return language;
	}

	protected void setLanguage(String language) {
		this.language = language;
//		LocaleUtil l = LocaleUtil.getInstance(); 
//		l.setLanguage(language);
	}

	public Settings copy() {
		Settings out = new Settings();
		out.setAiEnabled(this.isAiEnabled());
		out.setLanguage(this.getLanguage());
		return out;
	}

	public void restoreTo(Settings fromRestore) {
		this.setAiEnabled(fromRestore.isAiEnabled());
		this.setLanguage(fromRestore.getLanguage());
	}
	
	public static Settings getInstance(){
		return instance;
	}
}
