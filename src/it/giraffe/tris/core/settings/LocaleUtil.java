package it.giraffe.tris.core.settings;


import java.io.IOException;
import java.util.Hashtable;

import me.regexp.RE;

import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;


public class LocaleUtil {

	private static LocaleUtil instance = new LocaleUtil();	
	
	// FIXME language should be a variable of settings
	private static Hashtable<String, String> localeStrs;
//	private static String language;
	private static Resources resources;
	

	private LocaleUtil(){
		// TODO
		// Load default language!
//		LocaleUtil.setResources(resources);
//		LocaleUtil.setLanguage("it");
		
		localeStrs = new Hashtable<String, String>();
		try {
			// FIXME resource name should be global defined and accessible
			// in any point of application
			resources = Resources.openLayered("/resources");
			
			LocaleUtil.localeStrs = resources.getL10N("Locale", Settings
					.getInstance().getLanguage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static LocaleUtil getInstance(){
		return instance;
	}
	
	/**
	 * 
	 * @return
	 * @deprecated Use Settings.getInstance().getLanguage() instead
	 */
	@Deprecated
	public /*static*/ String getLanguage() {
		return Settings.getInstance().getLanguage();
//		return language;
	}

	/**
	 * 
	 * @param language
	 * 
	 */
	public void setLanguage(String language) {		
		Settings.getInstance().setLanguage(language);		
		LocaleUtil.localeStrs = resources.getL10N("Locale", language);
		UIManager.getInstance().setBundle(resources.getL10N("Locale", language));
	}

	/**
	 * 
	 * @return 
	 */
	public /*static*/ Hashtable<String, String> getLocaleStrs() {
		return localeStrs;
	}
	
	/**
	 * This method allow to obtain the locale string based on its name.
	 * This works well for every locale string, but its the only one that
	 * works also on parameterized local name such one that use $
	 * @param name The key of l10n
	 * @param params Optional parameter that allow to substitute every '$'
	 * with one of the string contained in this array. The substitution 
	 * is done sequentially starting to the first element until no more '$'
	 * are found. If there are more elements in params then '$' the remaining ones
	 * are ignored.
	 * @return Return a string represent the text translated in the choosen 
	 * language.
	 */
	// FIXME This method is only useful when there are parameters.
	// So I should put them mandatory.
	public static String getLocalStringParametic(String name, String[] params) {
		String localeStr = localeStrs.get(name);		
		if (localeStr == null){
			try {
				throw new MissingTranslationException(name);
			} catch (MissingTranslationException e) {
				// TODO Manage Log
				//Log.p("Translation error: " + e.getMessage());
				//Log.getInstance().setFileURL("log.txt");
				return "Translation error: " + e.getMessage();
			}
		}
		RE re = new RE("[$]+");		
		String splittedOut[] = re.split(localeStr);
		
		String out = splittedOut[0];
		
		int l = 0;
		while (l < params.length){
			if (l < splittedOut.length) {
				out = out + params[l] + splittedOut[l + 1];
				l++;
			}
			else {
				// The eventually remained parameters will be ignored
				break;
			}
		}		
		return out;
	}
	
	public /*static*/ void setResources(Resources resources) {
		LocaleUtil.resources = resources;		
	}	
}
