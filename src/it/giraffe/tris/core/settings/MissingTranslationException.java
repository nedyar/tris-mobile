package it.giraffe.tris.core.settings;

public class MissingTranslationException extends Exception {
	public MissingTranslationException(String missingLocaleId) {
		super(missingLocaleId);
	}
}
