package it.giraffe.tris.core;

/**
 * Represent a tile of TicTacToe game
 * @author Stefano
 *
 */
public class MyTile {

	private int x;
	private int y;

	private int id;
	private TileValue value;

	/**
	 * Constructor of a tile with a particular id and a given position (x,y)
	 * with default value equals to EMPTY
	 * @param id 
	 * @param x
	 * @param y
	 */
	public MyTile(int id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.value = TileValue.EMPTY;
	}

	/**
	 * Constructor of a tile with a particular id and a given position (x,y)
	 * with a given value
	 * @param id
	 * @param x
	 * @param y
	 * @param value
	 */
	public MyTile(int id, int x, int y, TileValue value) {
		this(id, x, y);
		this.value = value;
	}

	/**
	 * 
	 * @return Returns the tile's id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @return Returns the tile's value.
	 */
	public TileValue getValue() {
		return value;
	}

	/**
	 * Change the tile's value to given one.
	 * @param currentValue
	 */
	public void setValue(TileValue currentValue) {
		this.value = currentValue;
	}

	/**
	 * Custom toString() method: "MyTile [x={@code x}, y={@code y},
	 * id={@code id}, value={@code value}]"
	 */
	@Override
	public String toString() {
		return "MyTile [x=" + x + ", y=" + y + ", id=" + id + ", value="
				+ value + "]";
	}

	/**
	 * 
	 * @return Returns the tile's x coordinate.
	 */
	public int getX() {
		return x;
	}

	/**
	 * 
	 * @return Returns the tile's y coordinate.
	 */
	public int getY() {
		return y;
	}	
}
