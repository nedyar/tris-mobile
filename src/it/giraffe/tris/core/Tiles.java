package it.giraffe.tris.core;

import it.giraffe.tris.dataStructures.DirectedCouple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents the board for TicTacToe game.
 * @author Stefano
 *
 */
public class Tiles {

	/**
	 * Dimension of board.
	 */
	public static final int DIMENSION = 3;
	private MyTile tiles[][];

	/**
	 * Constructs an empty board.
	 */
	public Tiles() {
		this.tiles = new MyTile[DIMENSION][DIMENSION];
		this.clearTiles();
	}

	/**
	 * Clears all tiles in the board
	 */
	public void clearTiles() {
		int progressiveId = 0;
		for (int i = 0; i < DIMENSION; i++) {
			for (int j = 0; j < DIMENSION; j++) {
				tiles[i][j] = new MyTile(progressiveId, i, j);
				progressiveId++;
			}
		}
	}

	/**
	 * 
	 * @param tileID The tile's id to retrive.
	 * @return Returns a tile by its id.
	 */
	public MyTile getTileById(Integer tileID) {
		for (int i = 0; i < DIMENSION; i++) {
			for (int j = 0; j < DIMENSION; j++) {
				MyTile myTile = tiles[i][j];
				if (myTile.getId() == tileID) {
					return myTile;
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @return Returns true if some player make tris on rows.
	 */
	public boolean checkRows() {
		boolean out = false;
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempRowValue = tiles[i][0].getValue();
			int j = 1;
			while (!tempRowValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempRowValue.equals(tiles[i][j].getValue())) {
				j++;
			}
			if (j == 3) {
				return true;
			}
		}
		return out;
	}
	
	public boolean checkRows(TileValue player) {
		boolean out = false;
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempRowValue = player;
			int j = 0;
			while (!tempRowValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempRowValue.equals(tiles[i][j].getValue())) {
				j++;
			}
			if (j == 3) {
				return true;
			}
		}
		return out;
	}
	
	public List<DirectedCouple<Integer>> getWinningRows() {
		List<DirectedCouple<Integer>> out = new ArrayList<DirectedCouple<Integer>>();
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempRowValue = tiles[i][0].getValue();
			int j = 1;
			while (!tempRowValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempRowValue.equals(tiles[i][j].getValue())) {
				j++;
			}
			if (j == 3) {
				out.add(new DirectedCouple<Integer>(tiles[i][0].getId(), tiles[i][2].getId()));
			}
		}
		return out;
	}
	

	/**
	 * 
	 * @return Returns true if some player make tris on columns.
	 */
	public boolean checkCols() {
		boolean out = false;
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempColValue = tiles[0][i].getValue();
			int j = 1;
			while (!tempColValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempColValue.equals(tiles[j][i].getValue())) {
				j++;
			}
			if (j == 3) {
				return true;
			}
		}
		return out;
	}
	
	public boolean checkCols(TileValue player) {
		boolean out = false;
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempColValue = player;
			int j = 0;
			while (!tempColValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempColValue.equals(tiles[j][i].getValue())) {
				j++;
			}
			if (j == 3) {
				return true;
			}
		}
		return out;
	}
	
	
	public List<DirectedCouple<Integer>> getWinningCols() {
		List<DirectedCouple<Integer>> out = new ArrayList<DirectedCouple<Integer>>();
		for (int i = 0; i < DIMENSION; i++) {
			TileValue tempColValue = tiles[0][i].getValue();
			int j = 1;
			while (!tempColValue.equals(TileValue.EMPTY) && j < DIMENSION
					&& tempColValue.equals(tiles[j][i].getValue())) {
				j++;
			}
			if (j == 3) {
				out.add(new DirectedCouple<Integer>(tiles[0][i].getId(), tiles[2][i].getId()));
			}
		}
		return out;
	}

	/**
	 * 
	 * @return Returns true if some player make tris on diagonals.
	 */
	public boolean checkDiags() {
		boolean out = false;
		TileValue tempColValue = tiles[0][0].getValue();
		int j = 1;
		while (j < DIMENSION && !tempColValue.equals(TileValue.EMPTY)
				&& tempColValue.equals(tiles[j][j].getValue())) {
			j++;
		}
		if (j == 3) {
			return true;
		}

		TileValue center = tiles[1][1].getValue();
		TileValue so = tiles[2][0].getValue();
		TileValue ne = tiles[0][2].getValue();

		if (!center.equals(TileValue.EMPTY) && center.equals(so)
				&& center.equals(ne)) {
			return true;
		}
		return out;
	}
	
	public boolean checkDiags(TileValue player) {
		boolean out = false;
		TileValue tempColValue = player;
		int j = 0;
		while (j < DIMENSION && !tempColValue.equals(TileValue.EMPTY)
				&& tempColValue.equals(tiles[j][j].getValue())) {
			j++;
		}
		if (j == 3) {
			return true;
		}

		TileValue center = tiles[1][1].getValue();
		TileValue so = tiles[2][0].getValue();
		TileValue ne = tiles[0][2].getValue();

		if (!center.equals(TileValue.EMPTY) && center.equals(so)
				&& center.equals(ne) && center.equals(player)) {
			return true;
		}
		return out;
	}
	
	public List<DirectedCouple<Integer>> getWinningDiagonals() {
		List<DirectedCouple<Integer>> out = new ArrayList<DirectedCouple<Integer>>();
		TileValue tempColValue = tiles[0][0].getValue();
		int j = 1;
		while (j < DIMENSION && !tempColValue.equals(TileValue.EMPTY)
				&& tempColValue.equals(tiles[j][j].getValue())) {
			j++;
		}
		if (j == 3) {
			out.add(new DirectedCouple<Integer>(tiles[0][0].getId(), tiles[2][2].getId()));
		}

		TileValue center = tiles[1][1].getValue();
		TileValue so = tiles[2][0].getValue();
		TileValue ne = tiles[0][2].getValue();

		if (!center.equals(TileValue.EMPTY) && center.equals(so)
				&& center.equals(ne)) {
			out.add(new DirectedCouple<Integer>(tiles[0][2].getId(), tiles[2][0].getId()));
		}

		return out;
	}

	/**
	 * 
	 * @param x The x coordinate of tile to retrieve.
	 * @param y The y coordinate of tile to retrieve.
	 * @return Returns the tile's id of a given position.
	 */
	public int getTileIdByPosition(int x, int y) {
		return tiles[x][y].getId();
	}
	
	/**
	 * 
	 * @param tileId The tile's id to retrieve.
	 * @return Returns the tile's value of a given id.
	 */
	public TileValue getTileValueById(Integer tileId) {
		return this.getTileById(tileId).getValue();
	}

	/**
	 * 
	 * @param x The x coordinate of tile to retrieve.
	 * @param y The y coordinate of tile to retrieve.
	 * @return Returns the tile's value of a given position.
	 */
	public TileValue getTileValueByPosition(int x, int y) {
		return tiles[x][y].getValue();
	}
	
	/**
	 * Changes value of a tile by its position.
	 * @param x The x coordinate of tile to change.
	 * @param y The y coordinate of tile to change.
	 * @param tileValue The tile value to set.
	 */
	public void setTileValueByPosition(int x, int y, TileValue tileValue) {
		tiles[x][y].setValue(tileValue);
	}

	/**
	 * Custom toString method.
	 */
	@Override
	public String toString() {
		try {
			String out = "[" + Arrays.toString(tiles[1]);
			for (int i = 1; i < DIMENSION; i++) {
				out = out + "\n" + Arrays.toString(tiles[i]);
			}
			out = out + "]";
			return out;
		} catch (IndexOutOfBoundsException iooe) {
			return "Tiles [tiles=" + Arrays.toString(tiles) + "]";
		}
	}
}
