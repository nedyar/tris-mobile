package it.giraffe.tris.core;

import java.util.ArrayList;
import java.util.List;

import it.giraffe.codename2.ProgramManager;
import it.giraffe.tris.core.ai.AIPlayer;
import it.giraffe.tris.core.ai.AIPlayerAlphaBeta;
import it.giraffe.tris.core.ai.TicTacToeAI;
import it.giraffe.tris.core.settings.Settings;
import it.giraffe.tris.dataStructures.DirectedCouple;
import it.giraffe.tris.utils.MathUtil;

/**
 * Manager of TicTacToe's board.
 * @author Stefano
 *
 */
public class TileManager {

	private int usedTile;
	private Tiles tiles;
	private TileValue lastPlayer;
	private MathUtil mathUtil;
	private Boolean gameInProgress;
	private TicTacToeAI ticTacToeAI;
	private AIPlayer aiPlayer;

	/**
	 * Constructor that create an empty board.
	 */
	public TileManager() {
		mathUtil = new MathUtil();
		ticTacToeAI = new TicTacToeAI();		
		initBoard();
	}

	/**
	 * This method clears the board setting it to initial state.
	 */
	public void initBoard() {
		gameInProgress = true;
		usedTile = 0;
		tiles = new Tiles();
		if (ticTacToeAI != null) {
			ticTacToeAI.clearBoard();
		}
		if (mathUtil.getRandomBoolean()) {
			lastPlayer = TileValue.CIRCLE;
		} else {
			lastPlayer = TileValue.CROSS;
		}
//		aiPlayer = new AIPlayerMinimax(tiles);
		aiPlayer = new AIPlayerAlphaBeta(tiles);
		if (lastPlayer.equals(TileValue.CIRCLE)
				&& ProgramManager.getInstance().getSettings().isAiEnabled()){
			aiPlayer.setSeed(lastPlayer);
			int move[] = aiPlayer.move();
			this.useTile(move[0], move[1]);
//			GuiManager.refreshBoard(tiles);
		}
		else {			
			aiPlayer.setSeed(TileValue.getOpposite(lastPlayer));
		}
	}

	/**
	 * Play a move using the given tile.
	 * @param tileId The id of the tile used to perform a move.
	 * @return Returns the value assigned to played tile, or {@code null} if the move is illegal.
	 */
	public TileValue useTile(Integer tileId) {
		TileValue out = null;

		if (gameInProgress) {
			MyTile myTile = tiles.getTileById(tileId);
			TileValue value = myTile.getValue();
			if (value.equals(TileValue.EMPTY)) {
				TileValue currentValue = TileValue
						.getOpposite(lastPlayer);
				lastPlayer = currentValue;
				myTile.setValue(currentValue);
	
				if (Settings.getInstance().isAiEnabled()) {
					// TODO use only one board!
					// Update matrix board in Ai class
					ticTacToeAI.setBoardValue(myTile.getX(), myTile.getY(), currentValue.getInt());
				}			
				usedTile++;

				return currentValue;
			}
		}

		return out;
	}
	
	/**
	 * Play a move using the given tile.
	 * @param x The x coordinate of the tile to play.
	 * @param y The y coordinate of the tile to play.
	 * @return Returns the value assigned to played tile, or {@code null} if the move is illegal.
	 */
	public TileValue useTile(Integer x, Integer y) {
		Integer tileId = this.getTileIdByPosition(x, y);
		if (tileId != null) {
			return this.useTile(tileId);
		}
		else {
			return null;
		}
	}

	/**
	 * 
	 * @return Returns true iff the player who made the last move wins.
	 */
	public boolean checkWin() {
		boolean out = false;
		if (tiles.checkRows() || tiles.checkDiags() || tiles.checkCols()) {
			return true;
		}
		return out;
	}
	
	public List<DirectedCouple<Integer>> getWinningsTiles(){
		List<DirectedCouple<Integer>> out = new ArrayList<DirectedCouple<Integer>>();
		out.addAll(tiles.getWinningRows());
		out.addAll(tiles.getWinningCols());
		out.addAll(tiles.getWinningDiagonals());
		return out;
	}

	/**
	 * 
	 * @return Returns true iff the game ends in a draw.
	 */
	public boolean checkTie() {
		boolean out = false;
		if (!this.checkWin() && usedTile == (Tiles.DIMENSION * Tiles.DIMENSION)) {
			return true;
		}
		return out;
	}

	/**
	 * 
	 * @return Returns the dimension of the board.
	 */
	public int getDimension() {
		return Tiles.DIMENSION;
	}


	/**
	 * Stop a game in progress.
	 */
	public void stopGame() {
		gameInProgress = false;
	}
	
	/**
	 * Getters of game in progress.
	 * @return Return true iff the game is currently in progress.
	 */
	public boolean isGameInProgress(){
		return this.gameInProgress;
	}

	
//	/* get the board value for position (i,j) */
//	public int getBoardValue(int i,int j) {
//		return ticTacToeAI.getBoardValue(i, j);
//	}

	/**
	 *  Calculate the best move for current token according to AI engine.
	 */
	public int []nextMove() {
//		return ticTacToeAI.nextMove(lastPlayer.getInt());
		return aiPlayer.move();
	}

	/**
	 * 
	 * @param x The x coordinate of the tile.
	 * @param y The y coordinate of the tile.
	 * @return Returns the tile's id of a given positions.
	 */
	public Integer getTileIdByPosition(int x, int y) {
		return tiles.getTileIdByPosition(x, y);
	}
	
	/**
	 * 
	 * @param tileId The id of the tile to check.
	 * @return Returns the value of a tile by its id.
	 */
	public TileValue getTileValueById(Integer tileId) {
		return tiles.getTileValueById(tileId);
	}
	
	public MyTile getTileById(Integer tileId){
		return tiles.getTileById(tileId);
	}
	
	
}
